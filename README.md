# huffman encoding

Hey!! Arslan Here!
# Description

This is the Huffman Encoding Project.

This project is based on C++ programming language (Console).

This is Compression and Decompression of data using huffman encoding.

It contains one driver and one header file. In Header file, there is all implementaion but in driver this is some functions which is related to filing.

## Data Structure Components
In this, we use different data structure components like 
1-Queue
2-Minheap
3-Recursion
4-Classes (OOP)
5-Arrays etc.


## Tests
I tried all the cases. All are successfully passed. But if you find any issue then don't forget to mention it.

## Installation
You can download the zip file or clone it to your pc. It will run on all c++ editors because it has no graphical interface. 

## Usage
Just copy paste files and use it.

## Project status
Completed!!
